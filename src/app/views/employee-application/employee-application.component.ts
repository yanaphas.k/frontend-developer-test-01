import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { EmployeeInfoElement } from 'src/app/commons/model/employee-information';
import { CustomUtils } from 'src/app/commons/utils';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-employee-application',
  templateUrl: './employee-application.component.html',
  styleUrls: ['./employee-application.component.scss'],
})
export class EmployeeApplicationComponent implements OnInit {
  private routeSub: Subscription;
  employeeForm: FormGroup;
  errorMessage: string = '';
  id: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.employeeForm = this.fb.group({
      employeeid: [''],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      birthdate: ['', Validators.required],
      employeetype: ['', Validators.required],
      startdate: ['', Validators.required],
      salary: ['', Validators.required],
      pvfrate: ['', Validators.required],
    });

    this.routeSub = this.route.params.subscribe((params) => {
      if (params['id']) {
        this.id = params['id'];
        this.fillEmployeeData(this.id);
      }
    });
  }

  ngOnInit(): void {}

  openConfirmDialog(e: any): void {
    e.preventDefault();
    let employeeData = this.formatData(this.employeeForm.value);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      minWidth: '450px',
      data: {
        employeeData: employeeData,
        employeeTotalWorkday: CustomUtils.getEmployeeTotalWorkDay(employeeData),
        employeeTotalPvf:
          CustomUtils.getEmployeeTotalProvidentFund(employeeData),
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.router.navigate(['/']);
      }
    });
  }

  fillEmployeeData(id: string) {
    let data = this.employeeService.getEmployees();
    let employee = data.find(
      (d: EmployeeInfoElement) => d.employeeid.toString() == id
    );
    let birthdate = moment(employee.birthdate, 'DD/MM/YYYY');
    let startdate = moment(employee.startdate, 'DD/MM/YYYY');

    employee.birthdate = new Date(birthdate.toDate());
    employee.startdate = new Date(startdate.toDate());

    this.employeeForm.patchValue(employee);
  }

  onBack() {
    this.router.navigate(['/']);
  }

  formatData(data: any) {
    data.startdate = moment(this.employeeForm.value.startdate).format(
      'DD/MM/YYYY'
    );
    data.birthdate = moment(this.employeeForm.value.birthdate).format(
      'DD/MM/YYYY'
    );
    return data;
  }

  onSubmit() {
    this.errorMessage = '';
    if (this.employeeForm.valid) {
      let data = this.formatData(this.employeeForm.value);
      if (this.id) {
        this.employeeService.updateEmployee(data);
      } else {
        this.employeeService.createEmployee(data);
      }
      this.router.navigate(['/']);
    } else {
      this.errorMessage = 'Please fill reqired information.';
    }
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
