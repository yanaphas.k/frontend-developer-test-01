import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeInfoElement } from 'src/app/commons/model/employee-information';
import { CustomUtils } from 'src/app/commons/utils';
import { EmployeeService } from 'src/app/services/employee/employee.service';
import * as moment from 'moment';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent implements OnInit {
  employeeData: EmployeeInfoElement;
  employeeTotalPvf: number;
  employeeTotalWorkday: string;
  employeeTotalInterest: number;
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private employeeService: EmployeeService
  ) {
    this.employeeData = this.data.employeeData;
    this.employeeTotalPvf = this.data.employeeTotalPvf;
    this.employeeTotalWorkday = this.data.employeeTotalWorkday;

    this.employeeTotalInterest = CustomUtils.getAnnualInterest(
      this.employeeData,
      this.employeeData.salary
    );
  }

  ngOnInit(): void {}

  confirmLeave() {
    this.employeeData.leavestatus = true;
    let today = moment(new Date()).format('DD/MM/YYYY');
    this.employeeData.leavedate = today;
    this.employeeService.updateEmployee(this.employeeData);
    this.dialogRef.close(true);
  }
}
