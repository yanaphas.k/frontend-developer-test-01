import { EmployeeInfoElement } from './model/employee-information';
import * as moment from 'moment';

export class CustomUtils {
  static getWorkingYear(employeeStartDate: any) {
    let currentDate = moment();
    return moment.duration(currentDate.diff(employeeStartDate));
  }

  static getEmployeeTotalWorkDay(data: EmployeeInfoElement) {
    if (!data.startdate) return '';
    let employeeStartDate = moment(data.startdate, 'DD/MM/YYYY');
    let workingYears = this.getWorkingYear(employeeStartDate);

    let total =
      workingYears.years() !== 0 && workingYears.years() >= 1
        ? workingYears.years() + ' Years '
        : '';
    total +=
      workingYears.months() !== 0 && workingYears.months() >= 1
        ? workingYears.months() + ' Months'
        : '';
    return total;
  }

  static getAnnualInterest(data: EmployeeInfoElement, salary: number) {
    let employeeStartDate = moment(data.startdate, 'DD/MM/YYYY');
    let workingYears = this.getWorkingYear(employeeStartDate);
    return salary * 0.02 * workingYears.years();
  }

  static getSummaryProvidentFundWithoutInterest(
    data: EmployeeInfoElement,
    isCompanySuppert = true,
    percentage?: number
  ) {
    let { startdate, salary, pvfrate } = data;
    let currentDate = moment();
    let employeeStartDate = moment(startdate, 'DD/MM/YYYY').add(3, 'M');
    let countMonth = moment
      .duration(currentDate.diff(employeeStartDate))
      .asMonths();
    let pvfRate = pvfrate * 0.01;
    pvfRate = salary * pvfRate;
    pvfRate = Math.floor(countMonth) * pvfRate;
    if (isCompanySuppert) {
      let supportFromCompany = salary * 0.1;
      supportFromCompany = Math.floor(countMonth) * supportFromCompany;
      if (percentage) supportFromCompany = supportFromCompany * 0.5;
      return pvfRate + supportFromCompany;
    } else {
      return pvfRate;
    }
  }

  static getProvidentFund(
    data: EmployeeInfoElement,
    isCompanySuppert = true,
    percentage?: number
  ) {
    if (!data.startdate || data.employeetype == 'Part-time') return 0;
    let sum = this.getSummaryProvidentFundWithoutInterest(
      data,
      isCompanySuppert,
      percentage
    );
    return sum + this.getAnnualInterest(data, data.salary);
  }

  static getEmployeeTotalProvidentFund(data: EmployeeInfoElement) {
    if (!data.startdate || data.employeetype == 'Part-time') return 0;
    let currentDate = moment();
    let employeeStartDate = moment(data.startdate, 'dd/mm/yyyy');
    let workingYears = moment
      .duration(currentDate.diff(employeeStartDate))
      .asYears();

    if (workingYears > 5) {
      return this.getProvidentFund(data);
    } else if (workingYears >= 3) {
      return this.getProvidentFund(data, true, 50);
    } else {
      return this.getProvidentFund(data, false);
    }
  }
}
