import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeApplicationComponent } from './views/employee-application/employee-application.component';
import { ManageEmployeeComponent } from './views/manage-employee/manage-employee.component';

const routes: Routes = [
  { path: 'employee-application', component: EmployeeApplicationComponent },
  { path: 'employee-application/:id', component: EmployeeApplicationComponent },
  { path: 'manage-employee', component: ManageEmployeeComponent },
  { path: '', redirectTo: '/manage-employee', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
