import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { EmployeeInfoElement } from 'src/app/commons/model/employee-information';
import { CustomUtils } from 'src/app/commons/utils';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';
import { EmployeeService } from 'src/app/services/employee/employee.service';

@Component({
  selector: 'app-manage-employee',
  templateUrl: './manage-employee.component.html',
  styleUrls: ['./manage-employee.component.scss'],
})
export class ManageEmployeeComponent implements AfterViewInit {
  employeeData: EmployeeInfoElement[] = [];
  displayedColumns: string[] = [
    'employeeid',
    'firstname',
    'lastname',
    'birthdate',
    'age',
    'startdate',
    'employeetype',
    'salary',
    'pvfrate',
    'pvftotal',
    'leavestatus',
    'edit',
    'delete',
  ];
  dataSource = new MatTableDataSource<EmployeeInfoElement>(this.employeeData);
  clickedRows = new Set<EmployeeInfoElement>();
  @ViewChild(MatPaginator) paginator: any;

  constructor(
    public dialog: MatDialog,
    private employeeService: EmployeeService,
    private router: Router
  ) {
    this.employeeData = this.employeeService.getEmployees();
    this.employeeData.forEach((d) => {
      d.pvftotal = this.getEmployeeTotalProvidentFund(d);
    });
    this.dataSource = new MatTableDataSource<EmployeeInfoElement>(
      this.employeeData
    );
  }

  openConfirmDialog(employeeData: EmployeeInfoElement): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      minWidth: '450px',
      data: {
        employeeData: employeeData,
        employeeTotalWorkday: CustomUtils.getEmployeeTotalWorkDay(employeeData),
        employeeTotalPvf: this.getEmployeeTotalProvidentFund(employeeData),
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.employeeData = this.employeeService.getEmployees();
        this.dataSource.data = this.employeeData;
      }
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getAge(data: EmployeeInfoElement) {
    let currentDate = moment();
    let employeeAge = moment(data.birthdate, 'dd/mm/yyyy');
    let age = moment.duration(currentDate.diff(employeeAge));
    return age.years();
  }

  getTotalProvidentFund() {
    return this.employeeData
      .filter((t) => !t.leavestatus)
      .map((t) => {
        return t.pvftotal;
      })
      .reduce((acc, value) => {
        return acc + value;
      }, 0);
  }

  getEmployeeTotalProvidentFund(data: EmployeeInfoElement) {
    if (!data.startdate || data.employeetype == 'Part-time') return 0;
    return CustomUtils.getEmployeeTotalProvidentFund(data);
  }

  navigateToApplication(id?: string) {
    if (id) {
      this.router.navigate([`employee-application/${id}`]);
    } else {
      this.router.navigate(['employee-application']);
    }
  }
}
