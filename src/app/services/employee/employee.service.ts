import { Injectable } from '@angular/core';
import { EmployeeInfoElement } from 'src/app/commons/model/employee-information';
import * as employeeData from 'src/assets/Employees.json';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  employeeInformation: any;

  constructor() {
    let jsonData: any = employeeData;
    this.employeeInformation = jsonData.default;
    localStorage.setItem(
      'employeeData',
      JSON.stringify(this.employeeInformation)
    );
  }

  getEmployees() {
    let data: any = localStorage.getItem('employeeData');
    this.employeeInformation = JSON.parse(data);
    return this.employeeInformation;
  }

  createEmployee(employeeData: EmployeeInfoElement) {
    let lastEmployee: EmployeeInfoElement = this.employeeInformation.pop();
    employeeData.employeeid = lastEmployee.employeeid + 1;
    this.employeeInformation.push(employeeData);
    localStorage.setItem(
      'employeeData',
      JSON.stringify(this.employeeInformation)
    );
  }

  updateEmployee(employeeData: EmployeeInfoElement) {
    let eIndex = this.employeeInformation.findIndex(
      (d: EmployeeInfoElement) => d.employeeid == employeeData.employeeid
    );
    this.employeeInformation[eIndex] = employeeData;
    localStorage.setItem(
      'employeeData',
      JSON.stringify(this.employeeInformation)
    );
  }
}
