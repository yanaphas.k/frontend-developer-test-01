export interface EmployeeInfoElement {
  employeeid: number;
  firstname: string;
  lastname: string;
  birthdate: string;
  startdate: string;
  employeetype: string;
  salary: number;
  pvfrate: any;
  pvftotal: number;
  leavestatus?: boolean;
  leavedate?: string;
}
